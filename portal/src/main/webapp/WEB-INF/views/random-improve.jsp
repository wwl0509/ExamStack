<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <base href="<%=basePath%>">

    <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
    Remove this if you use the .htaccess -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>ExamStack</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" href="<%=basePath%>resources/images/favicon.ico"/>
    <link href="resources/bootstrap/css/bootstrap-huan.css" rel="stylesheet">
    <link href="resources/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="resources/css/exam.css" rel="stylesheet" type="text/css">
    <link href="resources/css/style.css" rel="stylesheet">
    <style type="text/css">
        input[type="radio"] {
            font-size: 100px;
        }

        .answer-desc {
            display: none;
        }
        .answer-desc span{
            color: red;
        }
    </style>
</head>
<body>
<header>
    <div style="display: none" class="container">
        <div class="row">
            <div class="col-xs-5">
                <div class="logo">
                    <h1><a href="#"><img alt="" src="resources/images/logo.png"></a></h1>
                </div>
            </div>
            <div class="col-xs-7" id="login-info">
                <c:choose>
                    <c:when test="${not empty sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.username}">
                        <div id="login-info-user">

                            <a href="user-detail/${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.username}"
                               id="system-info-account"
                               target="_blank">${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.username}</a>
                            <span>|</span>
                            <a href="j_spring_security_logout"><i class="fa fa-sign-out"></i> 退出</a>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <a class="btn btn-primary" href="user-register">用户注册</a>
                        <a class="btn btn-success" href="user-login-page">登录</a>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</header>
<!-- Navigation bar starts -->

<div  style="display: none" class="navbar bs-docs-nav" role="banner">
    <div class="container">
        <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
            <ul class="nav navbar-nav">
                <li>
                    <a href="home"><i class="fa fa-home"></i>主页</a>
                </li>
                <li>
                    <a href="student/practice-list"><i class="fa fa-edit"></i>试题练习</a>
                </li>
                <li>
                    <a href="exam-list"><i class="fa  fa-paper-plane-o"></i>在线考试</a>
                </li>
                <li>
                    <a href="training-list"><i class="fa fa-book"></i>培训资料</a>
                </li>
                <li>
                    <a href="student/usercenter"><i class="fa fa-dashboard"></i>会员中心</a>
                </li>
                <li>
                    <a href="student/setting"><i class="fa fa-cogs"></i>个人设置</a>
                </li>
            </ul>
        </nav>
    </div>
</div>

<!-- Navigation bar ends -->

<div class="content" style="margin-bottom: 100px;">

    <div class="container">
        <div class="row">
            <div class="col-xs-2" style="padding-right: 0px;padding-bottom:15px;">
                <div class="def-bk" id="bk-exam-control">

                    <div class="def-bk-content" id="exam-control">

                        <div id="question-time" style="display: none" class="question-time-normal">
                            <div style="height:140px;text-align: center;">
                                <i id="time-icon" class="fa fa-clock-o"> </i>
                            </div>

                            <span style="margin-right:10px;color: #B8B8B8;">已用时</span>
                            <span id="exam-clock">&nbsp;</span>
                            <span id="exam-timestamp" style="display:none;">${duration}</span>
                            <div id="answer-save-info"></div>

                        </div>
                        <div id="myquestion-submit">
                            <button class="btn-success btn" id="jiaojuan" data-toggle="modal" data-backdrop="false"	 data-target="#myModalQueRen" style="width:100%;">
                                我要交卷
                            </button>
                             <button class="btn-success btn" id="chongxinchujian" style="width:100%;display: none">
                                重新出卷
                            </button>

                        </div>
                        <div id="exam-info" style="display:none;">
                            <span id="answer-hashcode"></span>
                        </div>
                    </div>

                </div>

            </div>
            <div class="col-xs-10" style="padding-right: 0px;">
                <div class="def-bk" id="bk-exampaper">

                    <div class="expand-bk-content" id="bk-conent-exampaper">
                        <div id="exampaper-header">
                            <div id="exampaper-title">
                                <i class="fa fa-send"></i>
                                <span id="exampaper-title-name"> 模拟试卷 </span>
                                <span style="display:none;" id="exampaper-id">1</span>
                            </div>
                            <div id="exampaper-desc-container">
                                <div id="exampaper-desc" class="exampaper-filter">

                                </div>
                            </div>

                        </div>
                        <input type="hidden" id="start-time" value="${startTime }">
                        <input type="hidden" id="hist-id" value="${examHistoryId }">
                        <input type="hidden" id="paper-id" value="${examPaperId }">
                        <input type="hidden" id="exam-id" value="${examId }">
                        <ul id="exampaper-body">
                            ${htmlStr }
                        </ul>
                        <div id="exampaper-footer">
                            <div id="question-navi">
                                <div id="question-navi-controller">
                                    <i class="fa fa-arrow-circle-down"></i>
                                    <span>答题卡</span>
                                </div>
                                <div id="question-navi-content">
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
<footer>
    <div  style="display: none" class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="copy">
                    <p>
                        ExamStack Copyright © <a href="http://www.examstack.com/" target="_blank">ExamStack</a> - <a
                            href="." target="_blank">主页</a> | <a href="http://www.examstack.com/"
                                                                 target="_blank">关于我们</a> | <a
                            href="http://www.examstack.com/" target="_blank">FAQ</a> | <a
                            href="http://www.examstack.com/" target="_blank">联系我们</a>
                    </p>
                </div>
            </div>
        </div>

    </div>

</footer>

<!-- 模态框（Modal） -->
<div class="modal fade" id="myModalQueRen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="false">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    提示
                </h4>
            </div>
            <div class="modal-body">
               <h5>确认交卷吗？</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
                <button id="btnTijiao" type="button" class="btn btn-primary">
                    提交
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<!-- 模态框（Modal） -->
<div class="modal fade" id="myModalDaTi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="false">
                    &times;
                </button>
                <h4 class="modal-title" >
                    答题结果
                </h4>
            </div>
            <div class="modal-body">
               <h5><span id="datijieguo"></span></h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
<!-- Slider Ends -->

<!-- Javascript files -->
<!-- jQuery -->
<script type="text/javascript" src="resources/js/jquery/jquery-1.9.0.min.js"></script>

<!-- Bootstrap JS -->
<script type="text/javascript" src="resources/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/all.js?v=0712"></script>
<script type="text/javascript" src="resources/js/paper-examing.js"></script>

<script>

    function mygenrateAnswerSheet() {
        //		var as = new Array();
        /*var as = {};*/
        var answerSheetItems = new Array();
        var questions = $(".question");
        var zhengque=0;
        var weida=0;
        var cuowu=0;

        for (var i = 0; i < questions.length; i++) {
            var answerSheetItem = new Object();

            if ($(questions[i]).hasClass("qt-singlechoice")) {
                var radio_checked = $(questions[i]).find("input[type=radio]:checked");
                var radio_all = $(questions[i]).find("input[type=radio]");
                if (radio_checked.length == 0) {
                    answerSheetItem.answer = "";
                } else {
                    var current_index = $(radio_all).index(radio_checked);
                    answerSheetItem.answer = String.fromCharCode(65 + current_index);
                }
                answerSheetItem.questionTypeId = 1;
            } else if ($(questions[i]).hasClass("qt-multiplechoice")) {

                var checkbox_checked = $(questions[i]).find("input[type=checkbox]:checked");
                var checkbox_all = $(questions[i]).find("input[type=checkbox]");
                if (checkbox_checked.length == 0) {
                    answerSheetItem.answer = "";
                } else {
                    var tm_answer = "";
                    for (var l = 0; l < checkbox_checked.length; l++) {
                        var current_index = $(checkbox_all).index($(checkbox_checked[l]));
                        tm_answer = tm_answer + String.fromCharCode(65 + current_index);
                    }
                    answerSheetItem.answer = tm_answer;
                }
                answerSheetItem.questionTypeId = 2;
            } else if ($(questions[i]).hasClass("qt-trueorfalse")) {

                var radio_checked = $(questions[i]).find("input[type=radio]:checked");
                var radio_all = $(questions[i]).find("input[type=radio]");
                if (radio_checked.length == 0) {
                    answerSheetItem.answer = "";
                } else {
                    var current_index = $(radio_all).index(radio_checked);
                    answerSheetItem.answer = (current_index == 0) ? "对" : "错";
                }
                answerSheetItem.questionTypeId = 3;
            } else if ($(questions[i]).hasClass("qt-fillblank")) {
                answerSheetItem.answer = $(questions[i]).find("textarea").val();
                answerSheetItem.questionTypeId = 4;
            } else if ($(questions[i]).hasClass("qt-shortanswer")) {
                answerSheetItem.answer = $(questions[i]).find("textarea").val();
                answerSheetItem.questionTypeId = 5;
            } else if ($(questions[i]).hasClass("qt-essay")) {
                answerSheetItem.answer = $(questions[i]).find("textarea").val();
                answerSheetItem.questionTypeId = 6;
            } else if ($(questions[i]).hasClass("qt-analytical")) {
                answerSheetItem.answer = $(questions[i]).find("textarea").val();
                answerSheetItem.questionTypeId = 7;
            }
            answerSheetItem.point = 0;
            answerSheetItem.questionId = $(questions[i]).find(".question-id").text();
            answerSheetItem.a=$(questions[i]).find(".answer_value").text();
            if(answerSheetItem.a === answerSheetItem.answer){
                zhengque++;
            }else{
                if(answerSheetItem.answer===""){
                    weida++;
                }else {
                    cuowu++;
                }
                $(questions[i]).find(".answer-desc").show();
            }

            /*			var tmpkey = $(questions[i]).find(".question-id").text();
                        var tmpvalue = answerSheetItem;

                        as[tmpkey] = tmpvalue;*/

            answerSheetItems.push(answerSheetItem);
        }
        $("#datijieguo").text(" 总题目:"+answerSheetItems.length+" 正确题目:"+zhengque+" 错误题目:"+cuowu+" 未答题目:"+weida);

        $('#myModalDaTi').modal('show')
        return answerSheetItems;
    }
    function toHHMMSS(timestamp) {
        var sec_num = parseInt(timestamp, 10);
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours < 10) {
            hours = "0" + hours;
        }
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        if (seconds < 10) {
            seconds = "0" + seconds;
        }
        var time = hours + ':' + minutes + ':' + seconds;
        return time;
    }
    function examTimeOut (int){
        clearInterval(int);
       // examing.finishExam();
    }
    function startTimer() {
        var timestamp = parseInt($("#exam-timestamp").text());
        var int = setInterval(function() {
            $("#exam-timestamp").text(timestamp);
            $("#exam-clock").text(toHHMMSS(timestamp));
            if(timestamp < 600){
                var exam_clock = $("#question-time");
                exam_clock.removeClass("question-time-normal");
                exam_clock.addClass("question-time-warning");
            }
            var period = timestamp % 60;
            //		console.log("period :" + period);
            // if(period == 0)
            //     examing.saveAnswerSheet();
            timestamp-- || examTimeOut(int);
        }, 1000);
    }

    function jiaojuan(){
        $('#jiaojuan').hide();
        $('#chongxinchujian').show();
        clearInterval(examing.timerHandle);
        $('#myModalQueRen').modal('hide');
        mygenrateAnswerSheet();
    }

    $("#btnTijiao").click(function () {
       // if (confirm("确认交卷吗？11111")) {
        jiaojuan();


      //  }
      //  return false;
    });
    $("#chongxinchujian").click(function () {
      if(opener) opener.location.reload();
      if(parent) parent.location.reload();
        window.location.reload();
    });
    examing.finishExam=jiaojuan;

</script>


</body>
</html>
